/* José Sánchez Moreno - 23/01/14 - v1

Funcionalitats varies generals per a paraules
*/
package crecepalabra;

public class CjtParaules {
    
    protected Paraula[] paraules;
    protected int longitud;
    private static final int MAX_VALUE = 70;
    
    
    /**
     * Crea un conjunt de paraules buid.
     */
    public CjtParaules() {
        this.paraules = new Paraula[MAX_VALUE];
        longitud = 0;
    }
    
    
    //getters-setters-----------------------------------------------------------
    public Paraula getDarreraParaula() {
        if(0 == longitud) return null;
        return paraules[longitud-1];
    }
    
    protected boolean existeix(Paraula p) {
        for(int i=0; longitud > i; i++){
            if(p.equals( paraules[i]) )
                return true;
        }
        return false;
    }
    
    public void afegeix(Paraula p){
        this.paraules[longitud++] = p;
    }
    
    //public int longitud() {return this.longitud;}
    
    /**/
}
