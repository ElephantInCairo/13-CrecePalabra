/** José Sánchez Moreno - 23/01/14 - v1
 * 
 * Classe Fitxa, té valors segons li indiquis d'un idioma o un altre
 */

package crecepalabra;

public class Fitxa {
    private boolean usada;
    private char valor;
    
    
    //constructor
    public Fitxa(char e) {
        usada = false;
        
        switch(e) {
            case 'e': //español
                valor = (char) ( 97 + (int)(Math.random()*27) );
                
                if( (int)valor == 123 ) 
                    valor = (char) 241; //ñ
                break;
                
            case 'c': //catalan
                valor = (char) ( 97 + (int)(Math.random()*27) );
                
                if( (int)valor == 123 )
                    valor = (char) 231; //ç
                break;
                
            default: //ingles + altres (per defecte)
                valor = (char) ( 97 + (int)(Math.random()*26) );
                break;
        }  
    }
    
    //setters
    public void canviUs() { this.usada = !usada; }
    public void canviValor(char c) { this.valor = c; }
    
    //getters
    public boolean usada() { return usada; }
    public char getValor() { return this.valor; }
    
}
