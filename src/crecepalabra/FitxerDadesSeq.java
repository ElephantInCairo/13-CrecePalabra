/** José Sánchez Moreno - 23/01/14 - v1
 *
 * Aquesta classe maneja el/s diccionari/s.
 * 
 * Llegeix d'un fitxer mantenguent i reiniciant el punter a gust i permet també
 * afegir a final de fitcher.
 * 
 * En cas de intentar llegir un diccionari que no existeix el crea, així es 
 * poden crear diccionaris d'altres idiomes a gust (encara que tendran el 
 * conjunt de caracters predefinit, és a dir, l'anglès).
 * 
 * Utilitza de separador els caracters corresponents als enters 10 i 13.
 * 
 * Els diccionaris els he passat per una funció de sorting antes d'entregar pero
 * les paraules noves s'afegeixen al final
 */

package crecepalabra;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 *
 * @author tegus
 */
public class FitxerDadesSeq {
    
    private final File f;
    InputStream input = null;
    Reader r;
    
    /**
     * 
     * @param f
     * @throws IOException 
     */
    public FitxerDadesSeq(File f) throws IOException {

        if(!f.exists())
            f.createNewFile();
        
        this.f = f;
        this.input = new FileInputStream(f);
        this.r = new InputStreamReader(input);
    }
    
    /**
     * Reobri el fitxer per iniciar una cerca desde el principi.
     */
    public void begining() {
        
        try {
            this.input = new FileInputStream(f);
            this.r = new InputStreamReader(input);
            
        } catch (FileNotFoundException ex) {}
    }
    
    /**
     * Retorna la següent paraula possible.
     * La darrera és una paraula buida quan arriba a final de fitxer
     * 
     * @return 
     */
    public Paraula read() {
        Paraula p = new Paraula();
        
        try {
            int in = r.read();
            
            //botar caracters raros
            while(in == 10 || in == 13) {
                in = r.read();
            }
            
            while(in != -1 && in != 10 && in != 13) {
                p.append( (char) in );
                in = r.read();
            }
            
        } catch (IOException ex) {
            return null;
        }
        
        return p;
    }
    
    /**
     * Afegeix a final de fitxer.
     * @param p 
     */
    public void append(Paraula p) {
        
        try {
            
            FileWriter fileWritter = new FileWriter(f.getName(), true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            
            for(int i=p.longitud()-1, j = 0; j <= i; j++ ) {
                bufferWritter.write( p.getLletra(j) );
            }

            bufferWritter.write('\n'); //Nova línia
            
            bufferWritter.close();
            
        } catch (IOException ex) {}
    }
    
    /**
     * Reset de fitxers, els borra i crea buids
     */
    public void reset() {
        
        try {
            
            if( f.exists() )
                f.delete();
            
            f.createNewFile();
         
        } catch (IOException ex) {}
    
    }
    /**/
}
