/** José Sánchez Moreno - 03/09/14 - v1
 *
 * Arxiu amb unes quantes proves
 */

package crecepalabra;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 *
 * @author tegus
 */
public class Test {
    
    public static void main(String[] args){
        
        //test de lectura de fitxers sequencials--------------------------------
        /*
                
        File f = new File("diccionari_e.txt");
        FitxerDadesSeq fd = new FitxerDadesSeq(f);
        
        Paraula p;
        int i = 6;
        int j;
        while(i > 0) {
            j = i;
            while(j > 0){
                j--;
                p = fd.read();
                System.out.println(p);
            }
            System.out.println();
            fd.begining();
            i--;
        }
        /**/
        
        //test del joc en base a una sola paraula-------------------------------
        
        Game joc = new Game();
        Paraula n;
        boolean fi = false;
        String msg;

        Paraula b = llegeixParaula("Paraula base: ");
        
        while( !fi ) {
        
            System.out.println();
            n = llegeixParaula("Compara amb: ");
            
            System.out.print(b +" against "+ n +": ");
            msg = "Cap coincidencia";
            
            //casos en els que afegesc una lletra
            if( n.longitud() == b.longitud() + 1) {
                if( b.lletraMesOrdre(n) ) 
                    msg = "FitxaMesOrdre";
                if( b.lletraMes(n) )      
                    msg = "FitxaMes";
            }
            
            //casos en que mantenc el num de lletres
            else if( n.longitud() == b.longitud() ) {
                if( b.canviaLletraOrdre(n) ) 
                    msg = "CanviaLletraOrdre";
                if( b.canviaLletra(n) )    
                    msg = "CanviaLletra";
                if( b.ordre(n) )             
                    msg = "Ordre";
            }
            
            //altres casos
            else {
                msg = "ParaulaInvalida";
            }
            
            System.out.println(msg);
            System.out.println("vella:"+ b.charNotFound(n) +" nova:"+ n.charNotFound(b));
            
            
            if( n.toString().equals("") )
                fi = true;
        }
        
        System.out.println("FI");
        /**/
        
        //imprimint alguns caracters--------------------------------------------
        /*
        int a = 10;
        int b = 13;
        
        System.out.print((char)a);
        System.out.print("----");
        System.out.print((char)a);
        /**/
        
        //test de operacions de la classe paraula-------------------------------
        /*
        Paraula p;
        boolean fi = false;
        String msg;
        

        Paraula q = llegeixParaula("Paraula base: ");
        
        
        while( !fi ) {
        
            System.out.println();
            
            p = llegeixParaula("Compara amb: ");
            System.out.print(p.maxChar());
            
            System.out.print(q +" against "+ p +": ");
            msg = "Cap coincidencia";
            
            //casos en els que afegesc una lletra
            if( p.longitud() == q.longitud() + 1) {
                if( q.lletraMes(p) )  
                    msg = "FitxaMes";
                else if( q.lletraMesOrdre(p) )
                    msg = "FitxaMesOrdre";
            }
            
            //casos en que mantenc el num de lletres
            else if( p.longitud() == q.longitud() ) {
                if( q.canviaLletra(p) )
                    msg = "CanviaLletra";
                else if( q.ordre(p) )
                    msg = "Ordre";
                else if( q.canviaLletraOrdre(p) ) 
                    msg = "CanviaLletraOrdre";
                }
            
            //altres casos
            else {
                msg = "ParaulaInvalida";
            }
            
            System.out.println(msg);
            
            System.out.println("vella:"+ q.charNotFound(p) +" nova:"+ p.charNotFound(q));
            
            if( p.toString().equals("") )
                fi = true;
        }
        
        System.out.println("FI");
        
        */
        
        //test output-----------------------------------------------------------
        // Al final NO he optat per aquesta opció a l'hora de crear els nous 
        //  fichers
        /*
        File a = new File("prova.txt");
        Paraula p = llegeixParaula("Paraula: ");
        
        FitxersOrganitzats.escriu(a, p);
        
        /**/
        
        //test ñ ç -------------------------------------------------------------
        /*
        char c = (char)231;
        char d = (char)241;
        
        Paraula p = llegeixParaula("Prova 'ç ñ': ");
        
        System.out.println(p);
        System.out.println(c +""+ d);
        System.out.println( (char)231 +""+ (char)241 );
        /**/
        
        //fitxesDisponibles-----------------------------------------------------
        /*
        CjtFitxes cf = new CjtFitxes(5);
        cf.reparteix('e');    
        System.out.print(cf.getFitxes());
        
        Paraula p = llegeixParaula("P:");
        
        if(-1 < cf.disponibles(p.getLletra(0)))
            System.out.print("si ha trobat la lletra");
        System.out.print("no ha trobat la lletra");
        /**/
        
        
    }
    
    
    /**
     * Retorna una paraula a partir de l'entrada de teclat.
     * @return 
     */
    private static Paraula llegeixParaula(String s) {
        Paraula p = new Paraula();
        System.out.print(s);
        
        try{
            Reader in = new InputStreamReader(System.in);
            int c = in.read();
            while( c != 10 && c != -1 ){
                p.append( (char) c );
                c = in.read();
            }
            return p;
        } catch (IOException ex) { return null; }     
    }
    /**/
}
