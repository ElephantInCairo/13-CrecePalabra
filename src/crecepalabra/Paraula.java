/** José Sánchez Moreno - 11/08/14 - v1
 *
 * Classe proposada a classe.
 *
 * Com que maneja caràcters per formar una paraula s'han d'anar afegint de'n
 * un en un.
 */

package crecepalabra;

public class Paraula {
    
    private static final int MAX_VALUE = 35;
    private final char[] lletres;
    private int longitud;
    
    
    /**
     * Retorna una paraula buida.
     * 
     */
    public Paraula() {
        this.lletres = new char[MAX_VALUE];
        this.longitud = 0;
    }
    
    /**
     * Mètode que serveix per obtenir una Paraula igual a la que ja teniem.
     * Clonació de paraules
     * 
     * @param p 
     */
    public Paraula(Paraula p) {
        lletres = new char[p.longitud()];
        longitud = p.longitud();
        for(int i=0; longitud > i; i++){
            lletres[i] = p.getLletra(i);
        }
    }
    
    /**
     * 
     * @param s
     */
    public Paraula(String s){
        lletres = new char[s.length()];
        longitud = s.length();
        for(int i=0; longitud > i; i++){
            lletres[i] = s.charAt(i);
        }
    }
    
    
    //getters-------------------------------------------------------------------
    /**
     * Comparador.
     * @param q
     * @return 
     */
    public boolean equals(Paraula q) {
        if(longitud != q.longitud) return false;
        for(int i=0; longitud > i; i++){
            if(lletres[i] != q.lletres[i]) 
                return false;
        }
        return true;
    }
    
    public int longitud() { return this.longitud; }
    
    public char getLletra(int pos) {
        if(pos < longitud && pos >= 0) 
            return lletres[pos];
        return ' ';
    }
    
    @Override
    public String toString() {
        String s = "";
        for(int i=0; this.longitud > i; i++) {
            s += this.lletres[i];
        }
        return s;
    }
    
    //setters-------------------------------------------------------------------
    public void append(char c) {
        if (this.longitud < MAX_VALUE) 
            this.lletres[this.longitud++] = c;
    }
    
    
    //operacions----------------------------------------------------------------
    /**
     * Comprova que tenguin la mateixa mida i que només hi hagui una lletra
     * diferent (en el mateix ordre).
     * @param q
     * @return 
     */
    public boolean canviaLletra(Paraula q) {
        
        //el tamany ha de ser el mateix
        if( longitud != q.longitud )
            return false;
        
        int trobades = 0;
        
        for(int i=0; longitud > i; i++) {
            
            if( lletres[i] == q.lletres[i] ) 
                trobades++;
        
        }
        
        //s'han trobat totes menos una
        return ++trobades == longitud;
    }
    
    /**
     * Comprova que tenguin la mateixa mida i que totes les lletres d'una
     * estiguin també a l'altre.
     * @param q
     * @return 
     */
    public boolean ordre(Paraula q) {
        
        //el tamany ha de ser el mateix
        if( longitud != q.longitud )
            return false;
        
        Paraula r = new Paraula(q);
        int trobades = 0;
        
        for(int i = 0; longitud > i; i++) {
            
            for(int j = 0; r.longitud > j; j++) {
                
                if(lletres[i] == r.lletres[j]) {
                    r.lletres[j] = ' ';
                    trobades++;
                    break;
                }
            }
        }
        
        return trobades == longitud;
    }
    
    /**
     * Comprova que tenguin la mateixa mida i que totes les lletres d'una
     * estiguin també a l'altre excepte una (que es la que canvia).
     * @param q
     * @return 
     */
    public boolean canviaLletraOrdre(Paraula q) {
        
        //el tamany ha de ser el mateix
        if( q.longitud != longitud )
            return false;
        
        Paraula r = new Paraula(q);
        int trobades = 0;
        
        for(int i = 0; longitud > i; i++) {
            
            for(int j = 0; r.longitud > j; j++) {
                
                if(lletres[i] == r.lletres[j]) {
                    r.lletres[j] = ' ';
                    trobades++;
                    //+System.out.print(r);
                    break;
                }
            }
        }
        
        return ++trobades == longitud;
    }
    
    /**
     * Comprova que la paraula que m'envien te una lletra més i que deixant de 
     * banda la lletra nova, les demés estan iguals a la que tenia
     * 
     * @param q nova
     * @return 
     */
    public boolean lletraMes(Paraula q) {

        //el tamany ha de ser el corresponent
        if( longitud +1 != q.longitud )
            return false;
        
        int j = 0; //index de la paraula curta i num de lletres de q que estan a p
        
        for(int i = 0; q.longitud > i; i++) {
            //recorr la paraula llarga...
            if( lletres[j] == q.lletres[i] ) {
                j++; //mentre siguin iguals ves passant la curta
            }
        }
        
        return ++j == q.longitud;
    }
    
    /**
     * Comprova que te una lletra de més i que te totes les lletres 
     * que la que tenia excepte la de més.
     * 
     * @param q nova
     * @return 
     */
    public boolean lletraMesOrdre(Paraula q) {
    
        //el tamany ha de ser el corresponent
        if( longitud +1 != q.longitud )
            return false;
        
        Paraula r = new Paraula(q);
        int trobades = 0;
        
        for(int i = 0; longitud > i; i++) {
            
            for(int j = 0; r.longitud > j; j++) {
                
                if(lletres[i] == r.lletres[j]) {
                    r.lletres[j] = ' ';
                    trobades++;
                    //+System.out.print(r);
                    break;
                }
            }
        }
        
        return ++trobades == r.longitud;
    }
    
    /**
     * Retorna EL PRIMER CARACTER de la paraula actual que no està a la paraula 
     * que li envies (o espai en cas de que hi siguin tots) sense importar l'ordre. 
     * //si retorna espai, la paraula actual és un subconjunt de la que envies.
     * //pensar que sempre retorna un caracter de la paraula actual (no enviada)
     * @param q
     * @return 
     */
    public char charNotFound(Paraula q) {
        
        Paraula r = new Paraula(q);
        int trobades = 0;
        char c;
        
        for(int i = 0; longitud > i; i++) {
            
            c = lletres[i];
            
            for(int j = 0; r.longitud > j; j++) {
                
                if(c == r.lletres[j]) {
                    r.lletres[j] = ' ';
                    trobades++;
                    break;
                }
            }
            
            if(trobades == i) 
                return c;
            
        }
        
        return ' ';
    }
    
    /**
     * Retorna el numero de vegades que apareix la lletra que més apareix.
     * 
     * @return 
     */
    public int maxChar() {
    
        char[] ll = new char[MAX_VALUE];
        byte[] freq = new byte[MAX_VALUE];
        boolean trobat = false;
        byte index = 0;
        
        for(int i=0; longitud > i; i++) {
            
            for(int j=index-1 ; 0 <= j; j--) {
                
                if(lletres[i] == ll[j]) {
                    freq[j]++;
                    trobat = true;
                    //System.out.print(i + lletres[i]);
                    break;
                }
            }
            
            if(!trobat) {
                freq[index] = 1;
                ll[index++] = lletres[i];
            }
            
            trobat = false;
        }
        
        if(index < 0) 
            return 0;
        
        byte max = freq[index-1];
        
        for(int j=index-2; 0 <= j; j--) {
            
            if(freq[j] > max) 
                max = freq[j];
          
        }
        
        return max;
    }
    
    /**/
}
