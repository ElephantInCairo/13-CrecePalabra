/** José Sánchez Moreno - 25/01/14 - v1
 * 
 * Està pensat per fitxers que en coneixem la seva organització
 * (Ex: primera fila -> idioma, segona fila -> num de fitxes, ...)
 * Accés directe
 * Realment no necessita instàncies, retorna Paraules del fitxer que li demanis
 */

package crecepalabra;
import java.io.*;

public class FitxersOrganitzats {
    
    /**
     * Retorna la linea l del fitxer que li especifiquis.
     * @param f
     * @param l
     * @return 
     * @throws java.io.IOException 
     */
    public static Paraula llegeix_linia(File f, int l) throws IOException {
        
        Paraula p = new Paraula();
           
        InputStream input = new FileInputStream(f);
        Reader r = new InputStreamReader(input);

        //bota les primeres l linies
        int in = r.read();
        for(int i=0; l > i && in != -1; ){
            if(in == 10) i++;
            in = r.read();
        }

        while(in != -1 && in != 10 && in != 13) {
            p.append( (char) in );
            in = r.read();
        }    
             
        return p;
    }
    
    
    /**
     * Crea un fitxer nou borrant l'anterior.
     * 
     * @param f
     * @param p 
     */
    public static void escriu(File f, Paraula p) {
        
        try {
            FileOutputStream output = new FileOutputStream(f);
            
            for(int i=p.longitud()-1, j = 0; j <= i; j++ ) {
                output.write( p.getLletra(j) );
            }
         
        } catch (IOException ex) {}
    }
}
