/** José Sánchez Moreno - 21/01/14 - v1
 * 
 * Variables de joc i funcions que depenen de elles.
 */
package crecepalabra;

import java.io.File;
import java.io.IOException;

public class Game {
    //propietats de configuració
    private char idioma;
    private byte numFitxes;
    private FitxerDadesSeq config;
    private FitxerDadesSeq record; //si--- get set
    private FitxerDadesSeq diccionari;
    
    //propietats del joc
    private boolean comodin;
    private int puntuacio;
    private CjtFitxes fitxes;
    private final CjtParaules paraulesUsades;
    
    public enum Resultat {
        PRIMERA     (false, false, 0),
        
        CANVI       (true,  true,  3), 
        ORDRE       (false, false, 5),
        ORDRECANVI  (true,  true,  1), 
        MES         (true,  false, 10),
        ORDREMES    (true,  false, 5),
        
        DICCIONARI  (false, false, -1),  
        USADA       (false, false, -2),
        OPERACIONS  (false, false, -3),
        FITXES      (false, false, -4);
        
        private final boolean aug;
        private final boolean dism;
        private final int punts;

        Resultat(boolean a, boolean d, int p) {
            aug = a;
            dism = d;
            punts = p;
        }
    }
    
    
    /**
     * Crea un joc a partir de la configuració.
     * Carrega fitxers y configuracions bàsiques necesàries per altres fitxers
     * (diccionari, numFitxes, record).
     */
    public Game() {
        
        try {
            
            //configuració--------------------------------------------
            this.config = new FitxerDadesSeq( new File("config.txt") );
            
            //el num. de fitxes es troba a la primera linea
            Paraula n_fitxes = config.read();
            this.numFitxes = Byte.valueOf( n_fitxes.toString() );
            
            //l'idioma es troba a la segona linea
            Paraula idioma = config.read();
            this.idioma = idioma.getLletra(0);
            
            //altres fitxers-------------------------------------------
            this.record = new FitxerDadesSeq( new File("record.txt") );
            
            this.diccionari = 
                new FitxerDadesSeq( new File("diccionari_"+ this.idioma +".txt") );
            
        } catch (NumberFormatException nfex) {
            System.out.print("Revisa l'arxiu de configuració (nombre de fitxes) "
                    + nfex.getMessage() );
            
        } catch (IOException ex) {
            System.out.print("El ficher de configuració no existeix");
        }
       
        //inicialitza variables
        this.puntuacio = 0;
        this.comodin = true;
        this.paraulesUsades = new CjtParaules();
        this.fitxes = new CjtFitxes(numFitxes);
        
        reparteixFitxes();
    }
    
    /**
     * Dona valors al conjunt de fitxes.
     * Se ha separat per si se volen tornar a repartir durant el joc
     * (FITXES)
     * 
     */
    private void reparteixFitxes() {
        
        //inicialitza les fitxes amb que jugarem
        fitxes.reparteix(idioma);
    }
    
    //getters-------------------------------------------------------------------
    public boolean teComodin() {
        return this.comodin;
    }
    
    /**
     * Comodin que retorna les paraules vàlides.
     * 
     * @return 
     */
    public String comodin() {
        
        this.comodin = false;
        
        boolean tipus = ( null == paraulesUsades.getDarreraParaula() );
        Resultat r;
        String s = "";
        Paraula temp;
        //+int i = 0;
        
        this.diccionari.begining();
        temp = this.diccionari.read();
        
        while( 0 < temp.longitud() ) {
            
            r = (tipus) ? primeraParaula(temp) : seguentParaula(temp);
            
            if( r.compareTo(Resultat.DICCIONARI) < 0 )
                s += temp +"\n";
            
            //+System.out.println("+"+ i++ +temp);
            temp = this.diccionari.read();              
        }
        
        if( s.equals("") )
            return "No pots fer cap altre paraula\n";

        return "Palabras: \n"+ s;
    }
    
    /**
     * Comodin que te canvia les fitxes que no vols.
     * @param a antics
     * @param n nous
     * @return 
     */
    public boolean comodin(Paraula a, Paraula n) {
        
        if(n.longitud() > numFitxes / 5) return false;
            
        if ( fitxes.canviaValors(a, n) ) {
            this.comodin = false;
            return true;
        }
        
        return false;
    }
    
    /**
     * Retorna la darrera paraula que ha estat valida jugada.
     * (PARAULES)
     * 
     * @return 
     */
    private String getDarreraParaula() {
        Paraula p = paraulesUsades.getDarreraParaula();
        if( p == null ) return "-----";
        return p.toString();
    }
    
    /**
     * Mostra informació de la partida.
     * puntuacio, paraula anterior i fitxes restants.
     * 
     * @return 
     */
    public String Info() {
        
        String s = "\n";
        s += "Puntuació: "+ puntuacio +"   "+
             "Paraula anterior: "+ getDarreraParaula() +"\n";
        s += "Fitxes restants: "+ fitxes.noUsades().getFitxes();
        
        return s;
    }
    
    /**
     * Cerca el caracter c a les fitxes "NO_USADES".
     * (FITXES)
     * 
     * @param c
     * @return 
     */
    private boolean fitxaDisponible(char c) {
        return 0 <= fitxes.disponibles(c);
    }
    
    
    //setters-------------------------------------------------------------------
    /**
     * Cerca el caràcter c que estigui "USAT" i el marca com a "NO_USAT".
     * (FITXES)
     * 
     * @param c
     * @return 
     */
    private boolean aNoUsada(char c) {
        
        int pos;

        if( 0 > ( pos = fitxes.noDisponibles(c) ) )
            return false;
        
        fitxes.getFitxa(pos).canviUs();
        return true;
    }
    
    /**
     * Cerca el caràcter c que estigui "NO_USAT" i el marca com a "USAT".
     * (FITXES)
     * 
     * @param c
     * @return 
     */
    private boolean aUsada(char c) {
        
        int pos;
        
        if( 0 > ( pos = fitxes.disponibles(c) ) )
            return false;
        
        fitxes.getFitxa(pos).canviUs(); 
        return true;
    }
    
    /**
     * Canvia els caràcters de no usats a usats d'una paraula.
     * (Les fitxes estan "NO_USADES" perque es la primera paraula,
     * les fitxes hi son perque la paraula ha estat valida antes)
     * (PARAULES)
     * (FITXES)*
     * (AUGMENTA PUNTS)*
     * 
     * @param p 
     */
    public void aUsada(Paraula p) {
        
        for(int i=p.longitud()-1; 0 <= i; i-- ) {
            aUsada( p.getLletra(i) );
        }
        
        extras(p);
        
        paraulesUsades.afegeix(p);
    }
    
    /**
     * Canvia les fitxes que son necessàries d'acord amb la operació valida.
     * (AUGMENTA PUNTS)
     * 
     * @param r
     * @param p 
     */
    public void fitxes(Resultat r, Paraula p) {
        
        Paraula q = paraulesUsades.getDarreraParaula();
        
        if(r.aug)
            aUsada( p.charNotFound(q) );
        
        if(r.dism)
            aNoUsada( q.charNotFound(p) );
        
        extras(p);
        
        puntuacio += r.punts;
        paraulesUsades.afegeix(p);
    }
    
    /**
     * punts per altres factors.
     * (AUGMENTA PUNTS)
     * 
     * @param p 
     */
    private void extras(Paraula p) {
        
        if( 6 < p.longitud() ) 
            //puntua més a partir de 7 lletres
            puntuacio += 15*p.longitud();

        if( 2 < p.maxChar() )
            //una lletra tres pics punts+= ...
            puntuacio += 10;
        
    }
    
    
    //fitxers------------------------------------------------------------------- 
    /**
     * True si la paraula està al diccionari, false si no.
     * (DICCIONARI)
     * 
     * @param p
     * @return 
     */
    private boolean diccionari(Paraula p) {
        
        //paraules del diccionari
        Paraula temp; 
        this.diccionari.begining();
        
        do {
            temp = this.diccionari.read();
            
            if( p.equals(temp) )
                return true;
            
        } while( 0 < temp.longitud() );
    
        return false;
    }
    
    /**
     * Afegeix al final del diccionari.
     * (DICCIONARI)
     * 
     * @param p 
     */
    public void afegeixDiccionari(Paraula p) {
        diccionari.append(p);
    }
    
    /**
     * Regenera el fitxers tipus config o record
     * 
     * @param file
     * @param p
     * @param num 
     */
    private void newFile(FitxerDadesSeq file, Paraula p, Paraula num) {
        
        file.reset();
        file.append(p);
        file.append(num);
    }
    
    public void newConfig(Paraula p, Paraula num) {
        newFile(config, num, p);
    }
    
    public void newRecord(Paraula p) {
        newFile(record, p, new Paraula(""+puntuacio) );
    }
    
    public boolean record() {
        
        record.begining();
        record.read();
        
        byte n_record = Byte.valueOf( record.read().toString() );
        
        return n_record < puntuacio;
    }
    
    public String getRecord() {
        
        String s = "Record";
        
        record.begining();
        
        s += " de "+ record.read().toString();
        s += " ("+ record.read().toString() +" punts)";
        
        return s;
    }
    
    
    //funcionalitat-------------------------------------------------------------
    /**
     * Comprova que la paraula sigui un "subconjunt" de les fitxes.
     * Aquest mètode just es llança per formar la primera paraula.
     * 
     * @param p
     * @return 
     */ 
    private boolean disponible(Paraula p) {
        
        Paraula f = fitxes.toParaula();
        
        return ' ' == p.charNotFound(f);
    }
    
    
    /**
     * Depenguent de l'estat del joc comprova en base a la creació de la primera
     * paraula o segueix editant la anterior.
     * 
     * @param p
     * @return 
     */
    public Resultat comprova(Paraula p) {
        
        if( !diccionari(p) ) return Resultat.DICCIONARI;
        
        else if( null == paraulesUsades.getDarreraParaula() )
            return primeraParaula(p);
        
        else return seguentParaula(p);
    }
    
    /**
     * Mira si va be la primera paraula.
     * -disponible(fitxes)
     * 
     * @param p
     * @return zero (be) o menys dos (mal, fitxes...)
     */
    private Resultat primeraParaula(Paraula p) {
        
        // tenc les fitxes per poder fer-la
        if( disponible(p) )
            return Resultat.PRIMERA;
        
        return Resultat.FITXES;
    }
    
    /**
     * Retorna la puntuació obtinguda de la opció que li vagui be.
     * -no usada
     * 
     * @param p
     * @return 
     */
    private Resultat seguentParaula(Paraula p) {
        
        if( paraulesUsades.existeix(p) ) return Resultat.USADA;
        
        //obten la darrera paraula jugada (per comparar)
        Paraula q = paraulesUsades.getDarreraParaula();
        
        boolean fitxa = fitxaDisponible( p.charNotFound(q) );
        Resultat r = Resultat.OPERACIONS;
            
        //casos en que afegesc una lletra... (darrera +1)
        if( p.longitud() == q.longitud() + 1) {
            if( q.lletraMesOrdre(p) )      r = Resultat.ORDREMES;
            if( q.lletraMes(p) )           r = Resultat.MES;
        }

        //casos en que mantenc el num de lletres
        else if( p.longitud() == q.longitud() ) {
            if( q.canviaLletraOrdre(p) )   r = Resultat.ORDRECANVI;
            if( q.canviaLletra(p) )        r = Resultat.CANVI;
            if( q.ordre(p) )               return Resultat.ORDRE;
        }

        if( r != Resultat.OPERACIONS  && !fitxa ) 
            return Resultat.FITXES;
        return r;
    }
    
    /**/
}