/** José Sánchez Moreno - Programació I - Grup 3
 * 
 * Fco Perales, Gabriel Moyà.
 * 
 * Classe principal que guía el "fluxe" del joc.
 */

package crecepalabra;

import crecepalabra.Game.Resultat;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

public class CrecePalabra {

    public static void main(String[] args) {
                
        //variables auxiliars
        Game joc = new Game();
        boolean fi = false;
        Resultat r;
        Paraula p;
        Paraula a;
        Paraula n;
        
        System.out.println("Si necessites ajuda escriu \"/help\"");
        
        //bucle de joc
        while( !fi ) {
            
            //mostra la informació
            System.out.println( joc.Info() );
            
            //te demana una paraula
            p = llegeixParaula("Introdueix una paraula: ");
            
            switch( p.toString() ) {
                
                case "/exit": 
                    fi = true;
                    break;
                    
                case "/config": 
                    a = llegeixParaula("Idioma que vols usar [e/c/i/altres]: ");
                    n = llegeixParaula("Fitxes inicials: ");
                    joc.newConfig(a, n);
                    break;
                    
                case "/comodin": 
                    if( joc.teComodin() )
                        System.out.print( joc.comodin() );
                    break;
                    
                case "/comodin2":
                    a = llegeixParaula("lletres que vols llevar: ");
                    n = llegeixParaula("lletres que vols tenir: ");
                    if( !joc.comodin(a, n) ) {
                        System.out.print("No acompleixes algun dels requisits: "+
                                "\n-No tens alguna de les fitxes de "+ a +
                                "\n-Cas de canviar el mateix nombre de fitxes"+
                                "\n-No pots canviar mes de 1/5 de fitxes\n");
                    }
                    break;
                    
                case "/new":
                    joc = new Game();       
                    break;
                    
                case "/help": 
                    System.out.println("Comandos: \n"+
                            "/new: Juego nuevo\n/help: Ayuda\n/exit: Salir \n"+
                            "/config: Configurar\n/comodin: Ayuda en juego (pista)\n"+
                            "/comodin2: Ayuda en juego (cambio fichas)\n\n"+
                            "Juego: \nForma cadenas de palabras usando "+
                            "pequeñas variaciones sobre la anterior. \n"+
                            "Cambiar letra: 3p \nCambiar orden: 5p\n"+
                            "Cambiar orden y letra: 1p \nAñadir letra: 10p \n"+
                            "Añadir letra y cambiar orden: 5p\nBonificaciones "+
                            "a partir de siete letras o palabras que contengan "+
                            "una letra tres o mas veces");
                    break;
                    
                default:
                    
                    switch( r = joc.comprova(p) ) {
   
                        case PRIMERA:
                            joc.aUsada(p);
                            break;
                            
                        case CANVI:
                        case ORDRE:
                        case ORDRECANVI:
                        case MES:
                        case ORDREMES:
                            joc.fitxes(r, p);
                            break;
                            
                        case DICCIONARI:
                            System.out.println("La paraula "+ p +
                                    " no es troba al diccionari");
                            
                            if( pregunta("Vols afegir-la al diccionari?") ) 
                                joc.afegeixDiccionari(p);
                            break;
                            
                        case USADA:
                            System.out.println("La paraula "+ p +
                                    " ja ha estat usada");
                            break;
                            
                        case OPERACIONS:
                            System.out.println("La paraula "+ p +
                                    " no acompleix cap dels criteris");
                            break;
                            
                        case FITXES:
                            System.out.println("No disposes de les fitxes"+
                                    " per formar la paraula "+ p);
                            break;
                    }
                    
                    break;
            }
        }
        
        //Record
        if( joc.record() ) {
            System.out.println("Enhorabona, has batut el récord!");
            a = llegeixParaula("Introdueix el teu nom: ");
            joc.newRecord(a);
        }
        
        System.out.println( joc.getRecord() );
            
    }
    
    /**
     * Demana si/no.
     * 
     * @return 
     */
    private static boolean pregunta(String msg) {
        
        Paraula q = llegeixParaula(msg +" [s/n]: ");
        
        return ( q.getLletra(0) == 's' && q.longitud() == 1);
    }
    
    /**
     * Retorna una paraula a partir de l'entrada de teclat.
     * 
     * @return 
     */
    private static Paraula llegeixParaula(String msg) {
        Paraula p = new Paraula();
        System.out.print(msg);
        
        try{
            Reader in = new InputStreamReader(System.in);
            int c = in.read();
            
            while( c != 10 && c != -1 ){
                p.append( (char) c );
                c = in.read();
            }
            
            return p;
            
        } catch (IOException ex) { return null; }     
    }
    
    /**/
}
