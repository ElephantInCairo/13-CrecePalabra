/* José Sánchez Moreno - 23/01/14 - v1


*/
package crecepalabra;

public class CjtFitxes {
    
    private final Fitxa[] fitxes;
    public int longitud;
    static int MAX_VALUE;
    
    
    /**
     * CONSTRUCTOR. Crea un conjunt de fitxes de tamany n buid
     * 
     * @param n 
     */
    public CjtFitxes(int n) {
        MAX_VALUE = n;
        this.fitxes = new Fitxa[n];
        this.longitud = 0;
    }
    
    
    //getters-------------------------------------------------------------------
    /**
     * Retorna un CjtFitxes amb les fitxes que estan marcades sense usar.
     * //MostraInfo().
     * @return 
     */
    public CjtFitxes noUsades() {
        CjtFitxes nu = new CjtFitxes(longitud);
        for(int i=0; MAX_VALUE > i; i++){
            if( !fitxes[i].usada() )
                nu.afegeix(fitxes[i]);
        }
        return nu;
    }
    
    /**
     * Obte la fitxa d'una posicio de l'array.
     * Atencio amb que la posicio estigui fora de l'index
     * @param pos
     * @return 
     */
    public Fitxa getFitxa(int pos) {
        
        if(pos < 0 && pos > MAX_VALUE)
            return null;
        
        return this.fitxes[pos]; 
    }
    
    //public int longitud() { return this.longitud; }
    
    /**
     * Ve a ser un toString().
     * @return 
     */
    public String getFitxes() {
        
        String s = ""+ fitxes[0].getValor();
        
        for(int i=1; this.longitud > i; i++){
            s += ", "+ fitxes[i].getValor();
        }
        
        return s;
    }
    
    /**
     * Transforma el CjtFitxes a una paraula per poder operar amb les lletres.
     * S'utilitza només quan compara la primera paraula
     * 
     * @return 
     */
    public Paraula toParaula() {
        
        Paraula p = new Paraula();
        
        for(int i=0; MAX_VALUE > i; i++){
            p.append(fitxes[i].getValor());
        }
        
        return p;
    }
    
    //setters-------------------------------------------------------------------
    /**
     * Dona valors a les fitxes depenent de l'idioma que li passin.
     * 
     * @param e 
     */
    public void reparteix(char e) {
        for(int i=0; MAX_VALUE > i; i++)
            this.fitxes[i] = new Fitxa(e);
        this.longitud = MAX_VALUE;
    }

    private void afegeix(Fitxa f) {
        if (this.longitud < MAX_VALUE)
            this.fitxes[this.longitud++] = f; 
    }
    
    /**
     * Canvia el valor d'un subconjunt de fitxes
     * @param a
     * @param n
     * @return 
     */
    public boolean canviaValors(Paraula a, Paraula n) {
        
        //ha de canviar el mateix numero de fitxes
        if( a.longitud() != n.longitud() ) return false;
        
        //cerca que tenguis les fitxes antigues "no empleades"
        if( ' ' != a.charNotFound( this.noUsades().toParaula()) ) return false;
        
        for(int i=a.longitud()-1, p; 0 <= i; i-- ) { 
            //passa lletres de la nova
            char c = n.getLletra(i);
            //troba la posició de la antiga
            p = disponibles(a.getLletra(i));
            //canvia el valor de la antiga al de la nova
            fitxes[p].canviValor(c);
        }
        
        return true;
    }
        
    
    /*--- Gestió de fitxes usades/no usades --*/
    /**
     * Cerca fins que trobis el caracter "x" que estigui " sense usar".
     * Retorna la posició en que esteia o (-1) si no hi era.
     * @param c
     * @return 
     */
    public byte disponibles(char c) {
        for(int i=0, lon = this.longitud; lon > i; i++ ){
            if( fitxes[i].getValor() == c && !fitxes[i].usada() )
                return (byte) i;   
        }
        return -1;
    }
    
    /**
     * Cerca fins que trobis el caracter "x" que estigui "usat".
     * Retorna la posició en que esteia o (-1) si no hi era.
     * @param c
     * @return 
     */
    public byte noDisponibles(char c) {
        for(int i=0, lon = this.longitud; lon > i; i++ ){
            if( fitxes[i].getValor() == c && fitxes[i].usada() )
                return (byte) i;
        }
        return -1;
    }
}


