/* José Sánchez Moreno - 06/02/14 - v1

Recopilació de funcions que intervenen amb freqüencies de Paraules, BIBLIOTECA
*/
package crecepalabra;

public class FrequenciaLletres {
    
    static public int MAX_VALUE = 27;
    private int[] freq1 = new int[27];
    private int[] freq2 = new int[27];
    private static final char[] lletres = {'a', 'b', 'c', 'ç', 'd', 'e', 'f', 'g',
            'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o', 'p', 'q', 'r', 's',
            't', 'u', 'v', 'w', 'x', 'y', 'z'};

    
    /**
     * Treu les dades que podem obtenir de les dues paraules
     * @param p
     * @param q 
     */
    public FrequenciaLletres(Paraula p, Paraula q) {
        
        //realitza el recompte de cada lletra per les dues paraules
        for(int i=0; MAX_VALUE > i; i++) { //lletres
            
            for(int j=0; p.longitud() > j; j++){ //p & q
                
                if( p.getLletra(j) == lletres[i] ) 
                    freq1[i]++;
                
                if( q.getLletra(j) == lletres[i] ) 
                    freq2[i]++;
                
            }
        }
        
        //troba i fa el recompte dels caracters que tenen freq. diferent
        n_augments = 0;
        n_disminucions = 0;
        
        for(int i=0; MAX_VALUE > i; i++) {
            
            if(freq1[i] > freq2[i]) {
                augments[n_augments] = lletres[i];
                n_augments++;
                
            } else if(freq1[i] < freq2[i]) {
                disminucions[n_disminucions] = lletres[i];
                n_disminucions++;
            }            
        }
    }
    
    public FrequenciaLletres() {}
    

    public char[] augments     = new char[10];
    public char[] disminucions = new char[10];
    public int n_augments = 0;
    public int n_disminucions = 0;
    
    //getters-------------------------------------------------------------------
    public int n_augments()     { return n_augments;     }
    public int n_disminucions() { return n_disminucions; }
    
    public char augments(int i) {
        
        try{
            return augments[i];
        } catch(NullPointerException e){
            return ' ';
        }
    }
    
    public char disminucions(int i) {
        
        try{
            return disminucions[i];
        } catch(NullPointerException e){
            return ' ';
        }
    }
    
    /**
     * 
     * @param c
     * @return 
     */
    public int diferencia(char c) {
        
        for(int i=0; MAX_VALUE > i; i++) { //lletres

            if( c == lletres[i] )
                return Math.max(freq1[i] - freq2[i], freq2[i] - freq1[i]);
            
        }
        
        return -1;
    }
    
    /**
     * Retorna el numero de vegades en que apareix la lletra més frequent.
     * Apesar de que canvia els valors que apareixen a freq1, no altera els
     * resulats retornats a la darrera cerca.
     * @param p
     * @return 
     */
    public int maxChar(Paraula p) {
        int n = 0;
        
        for(int i=0; MAX_VALUE > i; i++) { //lletres
            
            for(int j=0; p.longitud() > j; j++) { //p
                
                if( p.getLletra(j) == lletres[i] ) 
                    if( ++freq1[i] > n)
                        n = freq1[i];
                
            }
        }
        
        return n;
    }
    
    
    //cas no hi ha canvis -
    //cas hi ha un canvi -
    //cas hi ha mes d'un canvi -
    //el cas ha augmentat en X vegades
    //el cas ha disminuit en X vegades
}
